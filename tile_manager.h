#pragma once
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include "lcd.h"
#include "utils.h"

static uint16_t tmp_buf[TILE_SIZE*TILE_SIZE];

uint16_t* load_tile(char* name);
uint16_t* load_tile_ppm(char* name);

void save_tile(uint16_t* tile, char* name);

void save_tmp(int x, int y, uint16_t color);

void save_tmp_tile(char* name);

uint16_t get_tmp_clr(int x, int y);
