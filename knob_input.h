#pragma once

#include <stdint.h>
#include "utils.h"

void init_knobs();

dir_t get_dir();

dir_t get_dir_abs_blue();

dir_t get_dir_abs_red();

int get_select();

bool is_pressed();

int get_val(int);
