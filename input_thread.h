#pragma once

#include <stdbool.h>
#include <pthread.h>
#include <termios.h>
#include <unistd.h>
#include <stdio.h>
#include "utils.h"

//extern enum dir_t dir;

//extern bool quit_all;
//extern bool enter_stroke;
//extern bool g_stroke;

//extern dir_t dir2;
//extern dir_t dir3;

extern pthread_mutex_t mtx;

void call_termios(int reset);

void* input_thread(void*);

