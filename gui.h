#pragma once

#include "lcd.h"

void gui_init();

void draw_background();

void gui_clear();

void draw_menu();

void draw_editor();

void draw_selector(int x, int y, uint16_t clr);

void draw_game_over(char *);

void draw_settings(int sel, int, int, int, int, int);
