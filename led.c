
#include "mzapo_phys.h"
#include "mzapo_regs.h"
#include "utils.h"
#include <stdint.h>

volatile uint8_t* mem_base;

void led_init() {
	mem_base = map_phys_address(SPILED_REG_BASE_PHYS, SPILED_REG_SIZE, 0);
	//mem_base + SPILED_REG_LED_LINE_o;
}

void led_line_clear() {

	*(uint32_t*) (mem_base + SPILED_REG_LED_LINE_o) = 0;
}

void led_line_right(uint8_t i) {
	if (i > 32)
		return;
	uint32_t a = 0;
	for (int j = 0; j < i; ++j)
		a += (1<<j);

	*(uint32_t*) (mem_base + SPILED_REG_LED_LINE_o) |= a;
}


void led_line_left(uint8_t i) {
	if (i > 32)
		return;
	uint32_t a = 0;
	for (int j = 0; j < i; ++j)
		a += (1<<(31-j));

	*(uint32_t*) (mem_base + SPILED_REG_LED_LINE_o) |= a;

}
