#pragma once

#include <stdlib.h>
#include "mzapo_parlcd.h"
#include <stdint.h>
#include "mzapo_phys.h"
#include "mzapo_regs.h"
#include "font_types.h"

#define GREEN 0b0000011111100000
#define RED   0b1111100000000000
#define BLUE  0b0000000000011111
#define TILE_SIZE 16
#define LCD_HEIGHT 320
#define LCD_WIDTH 480

//typedef uint16_t* tile;

static int scale;

extern font_descriptor_t *fdes;

int char_width(int ch);

void draw_char(int x, int y, char ch, unsigned short color);

void draw_pixel_big(int x, int y, unsigned short color);

void draw_pixel(int x, int y, uint16_t color);

void draw_tile(int x, int y, uint16_t *tile);

void draw_tile_color(int x, int y, uint16_t color);

void lcd_refresh();

void lcd_init();

void clear_screen();

void lcd_destroy();

void print(int x, int y, const char*, int s, uint16_t clr);
