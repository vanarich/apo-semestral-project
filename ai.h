#pragma once
#include "snake.h"
#include "utils.h"
#include <stdbool.h>
#include <time.h>


typedef enum {
    EASY_MODE,
    HARD_MODE
} game_mode_t;

void init_random();

poss_dir_t* possible_directions(snake_t *snake,snake_t *opp);

bool opposite_direction(dir_t dir1, dir_t dir2);

bool is_dir_possible(dir_t dir,snake_t *snake,snake_t *opp);

bool find_cell(int x, int y, snake_t *snake,snake_t *opp,game_mode_t mode);

bool snake_next_himself(snake_t *snake, dir_t dir);

bool snake_next_snake(snake_t *snake, snake_t *opp, dir_t dir);

dir_t choose_dir_prob(dir_t dir_r,game_mode_t mode,poss_dir_t* p);