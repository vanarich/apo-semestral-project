#include "player2.h"

void update_food(cell_t* food, snake_t *snake1, snake_t *snake2){
    //printf("start upd\n");
    bool good=false;
    while(!good){
        debug("start");
        food->x=rand() % 30;
        food->y=rand() % 20;
	food_tile_rand(food);
        good=true;
        //printf("here3\n");
        if(snake1!=NULL){
            //printf("here1\n");
            cell_t *head;
            head=snake1->head;
            for(int i=0;i<snake1->length;++i){
                //printf("%d\n",i);
                if(food->x==head->x && food->y==head->y){
                    good=false;
                    break;
                }
                head=head->next;
            }
        }
        if(snake2!=NULL && good){
            //printf("here2\n");
            cell_t *head;
            head=snake2->head;
            for(int i=0;i<snake2->length;++i){
                if(food->x==head->x && food->y==head->y){
                    good=false;
                    break;
                }
                head=head->next;
            }
        }
    }
    
}


bool snake_bite_snake(snake_t* snake1, snake_t* snake2){
    //printf("here40\n");
    bool ret=false;
    cell_t *head;
    head=snake1->head;
    cell_t *body;
    body=snake2->head->next;
    for (int i=1;i<snake2->length;++i){
		if((head->x == body->x) && (head->y == body->y)){
            ret=true;
            break;
        }
        body=body->next;
	}
    //printf("here50\n");
    return ret;
}

collision_t decode_collision(snake_t* snake1, snake_t* snake2,cell_t* food){
    collision_t ret=C_OTHER;
    if(snake1!=NULL){
        //printf("here20\n");
        cell_t *head1 = snake1->head;
        if((head1->x == food->x) && (head1->y == food->y)){
            ret=C_SNAKE1_FOOD;
        }else if(snake_bite_snake(snake1,snake1)){
            ret=C_SNAKE1;
        }
    }
    if(snake2!=NULL){
        //printf("here30\n");
        cell_t *head2 = snake2->head;
        if((head2->x == food->x) && (head2->y == food->y)){
            ret=C_SNAKE2_FOOD;
        }else if(snake_bite_snake(snake2,snake2)){
            ret=C_SNAKE2;
        }
    }
    if(snake1!=NULL && snake2!=NULL){
        cell_t *head1 = snake1->head;
        cell_t *head2 = snake2->head;
        if(snake_bite_snake(snake1,snake2)){
            ret=C_SNAKE1_to_SNAKE2;
        }else if(snake_bite_snake(snake2,snake1)){
            ret=C_SNAKE2_to_SNAKE1;
        }else if((head1->x == head2->x)&&(head1->y == head2->y)){
            ret=C_HEAD;    
        }
    }
    return ret;
    
}

/*works also as ai alone - second snake to NULL*/
bool ai_v_ai(snake_t* snake1, snake_t* snake2,cell_t* food,game_mode_t mode,cell_t **cell_buf,result_t* res,bool* fc){
    bool end=false;
    if (*fc){
        //printf("first cycle\n");
        if(snake1!=NULL){
            move_snake(snake1,right);
        }
        if(snake2!=NULL){
            move_snake(snake2,right);
        }
        draw_cell(food);
	    *fc = false;
        return end;
    }
    draw_cell(food);
    if(snake1!=NULL){
        //printf("snake1 motion\n");
        find_cell(food->x, food->y, snake1,snake2,mode);
        update_snake(snake1);
        draw_snake(snake1);
    }
    if(snake2!=NULL){
        //printf("snake2 motion\n");
        find_cell(food->x, food->y, snake2,snake1,mode);
        update_snake(snake2);
        draw_snake(snake2);
    }
    if(collision(cell_buf)){
        //printf("collision!\n");
        collision_t col = decode_collision(snake1,snake2,food);
        switch(col){
            case C_SNAKE1_FOOD:
                increase_length(snake1);
                update_food(food,snake1,snake2);
                break;
	        case C_SNAKE2_FOOD:
                increase_length(snake2);
                update_food(food,snake1,snake2);
                break;
            case C_SNAKE1:
            case C_SNAKE1_to_SNAKE2:
                *res=P2;
                end=true;
                break;
            case C_SNAKE2:
            case C_SNAKE2_to_SNAKE1:
                *res=P1;
                end=true;
                break;
            case C_HEAD:
                *res=D;
                end=true;
                break;
            default:
                break;
        }
        //printf("collision type %d, end %d\n",col,end);
    }
    return end;
}

bool player_v_ai(snake_t* snake1, snake_t* snake2,cell_t* food,dir_t dir,game_mode_t mode,cell_t **cell_buf,result_t* res,bool* fc){
    bool end=false;
    if (*fc){
        debug("first cycle");
        if(snake1!=NULL){
            move_snake(snake1,right);
        }
        if(snake2!=NULL){
            move_snake(snake2,right);
        }
        draw_cell(food);
	    *fc = false;
        debug("return end");
        return end;
    }
    draw_cell(food);
    debug("HERE1");
    if(snake1!=NULL){
        move_snake(snake1,dir);
    }
    debug("HERE2");
    if(snake2!=NULL){
        find_cell(food->x, food->y, snake2,snake1,mode);
        update_snake(snake2);
        draw_snake(snake2);
    }
    debug("HERE3");
    if(collision(cell_buf)){
        debug("collision!");
        collision_t col = decode_collision(snake1,snake2,food);
        switch(col){
            case C_SNAKE1_FOOD:
                increase_length(snake1);
                update_food(food,snake1,snake2);
                break;
	        case C_SNAKE2_FOOD:
                increase_length(snake2);
                update_food(food,snake1,snake2);
                break;
            case C_SNAKE1:
            case C_SNAKE1_to_SNAKE2:
                *res=P2;
                end=true;
                break;
            case C_SNAKE2:
            case C_SNAKE2_to_SNAKE1:
                *res=P1;
                end=true;
                break;
            case C_HEAD:
                *res=D;
                end=true;
                break;
            default:
                break;
        }
        //printf("collision type %d, end %d\n",col,end);
    }
    return end;
}

/*works also as player alone - second snake to NULL*/
bool player_v_player(snake_t* snake1, snake_t* snake2,cell_t* food,dir_t dir1,dir_t dir2,cell_t **cell_buf,result_t* res,bool* fc){
    bool end=false;
    draw_cell(food);
    if (*fc){
        debug("first cycle");
        if(snake1!=NULL){
            move_snake(snake1,right);
        }
        if(snake2!=NULL){
            move_snake(snake2,right);
        }
        draw_cell(food);
	    *fc = false;
        return end;
	}
    if(snake1!=NULL){
        move_snake(snake1,dir1);
    }
    if(snake2!=NULL){
        move_snake(snake2,dir2);
    }
    if(collision(cell_buf)){
        debug("collision!");
        collision_t col = decode_collision(snake1,snake2,food);
        debug("decoded");
        switch(col){
            case C_SNAKE1_FOOD:
                increase_length(snake1);
                update_food(food,snake1,snake2);
                break;
	        case C_SNAKE2_FOOD:
                increase_length(snake2);
                update_food(food,snake1,snake2);
                break;
            case C_SNAKE1:
            case C_SNAKE1_to_SNAKE2:
                *res=P2;
                end=true;
                break;
            case C_SNAKE2:
            case C_SNAKE2_to_SNAKE1:
                *res=P1;
                end=true;
                break;
            case C_HEAD:
                *res=D;
                end=true;
                break;
            default:
                break;
        }
        //debug("collision type %d, end %d\n",col,end);
    }
    return end;
}

