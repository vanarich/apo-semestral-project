#include "utils.h"
#include <stdlib.h>
//#define __DEBUG__

void* my_alloc(size_t size) {
	void* ret = malloc(size);
	if (ret == NULL) {
		fprintf(stderr, "Not enough memory.\n");
		exit(100);
	}
	return ret;
}

void debug(char* s) {
#ifdef __DEBUG__
	printf("DEBUG: %s\n", s);
#endif
	return;
}

