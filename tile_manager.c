#include "tile_manager.h"

//tmp_buf[TILE_SIZE*TILE_SIZE];

uint16_t* load_tile(char* name) {
	FILE *f;
	f = fopen(name, "rb");
	uint16_t *ret = my_alloc(2*TILE_SIZE*TILE_SIZE);
	//fread(NULL, 1, 6+4, 1, f);
	fread(ret, sizeof(uint16_t), TILE_SIZE*TILE_SIZE, f);
	//printf("%d\n", ret[0]);
	fclose(f);
	return ret;
}

uint16_t* load_tile_ppm(char* name) {
	//printf("entered\n");
	FILE *f;
	f = fopen(name, "rb");

	if (f == NULL) {
		printf("ERROR COULDNT LOAD FILE %s\n", name);
		return NULL;
	}

	//printf("opened\n");
	uint8_t arr[100000];
	uint16_t *ret = my_alloc(2*TILE_SIZE*TILE_SIZE);
	fread(arr, 1, 24*2+11, f);
	//printf("read 10 B\n");

	uint8_t pixel[3];

	for (int i = 0; i < TILE_SIZE*TILE_SIZE; ++i) {
		fread(pixel, sizeof(uint8_t), 3, f);
		pixel[0] = 32*pixel[0]/256;
		pixel[1] = 64*pixel[1]/256;
		pixel[2] = 32*pixel[2]/256;
		uint16_t clr = (pixel[0] << (5+6)) + (pixel[1] << 5) + pixel[2];
		ret[i] = clr;
		//printf("%d %d %d %d\n", pixel[0], pixel[1], pixel[2], clr);
	}
	//printf("%d\n", ret[0]);
	fclose(f);
	return ret;
}

void save_tile(uint16_t* tile, char* name) {
	FILE *f;
	f = fopen(name, "wb");
	fwrite(tile, sizeof(uint16_t), TILE_SIZE*TILE_SIZE, f);
	fclose(f);
}

void save_tmp(int x, int y, uint16_t color) {
	if (y < 0 || y > TILE_SIZE)
		return;
	if (y < 0 || y > TILE_SIZE)
		return;
	tmp_buf[x + y * TILE_SIZE] = color;
	//printf("wrote clr %d\n", color);
}

void save_tmp_tile(char* name) {
	save_tile(tmp_buf, name);
}

uint16_t get_tmp_clr(int x, int y) {
	return tmp_buf[x + TILE_SIZE*y];
}
