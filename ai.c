#include "ai.h"

void init_random(){
    srand(time(NULL)); 
}

bool snake_next_himself(snake_t *snake, dir_t dir){
    bool ret = false;
    cell_t *head;
    head=snake->head;
    cell_t *pom;
    pom=head->next;
    for(int i=1;i<snake->length;++i){
        switch(dir){
            case up:
                if(head->y==(pom->y) +1){
                    ret=true;
                }
                break;
            case down:
                if(head->y==(pom->y) -1){
                    ret=true;
                }
                break;
            case left:
                if(head->x==(pom->x)+1){
                    ret=true;
                }
                break;
            case right:
                if(head->x==(pom->x)-1){
                    ret=true;
                }
                break;
        }
        if(ret){
            break;
        }
        pom=pom->next;
    }
    return ret;
}

bool snake_next_snake(snake_t *snake, snake_t *opp, dir_t dir){
    bool ret=false;
    if(opp==NULL){
        ret=false;
    }else{
        cell_t *head=snake->head;
        cell_t *pom=opp->head;
        for(int i=0;i<opp->length;++i){
            switch(dir){
                case up:
                    if(head->y==(pom->y) +1){
                        ret=true;
                    }
                    break;
                case down:
                    if(head->y==(pom->y) -1){
                        ret=true;
                    }
                    break;
                case left:
                    if(head->x==(pom->x)+1){
                        ret=true;
                    }
                    break;
                case right:
                    if(head->x==(pom->x)-1){
                        ret=true;
                    }
                    break;
            }
            if(ret){
                break;
            }
            pom=pom->next;
       } 
    }
    return ret;
}

poss_dir_t* possible_directions(snake_t *snake,snake_t *opp){
    poss_dir_t* p;
    p=my_alloc(sizeof(poss_dir_t));
    p->dirs=my_alloc(4*sizeof(dir_t));
    p->num=0;
    int idx=0;
    for(dir_t i=up; i<=right; ++i){
        if(is_dir_possible(i,snake,opp)){
            p->dirs[idx++]=i;
            p->num++;
        }
    }
    return p;
}

dir_t choose_dir_prob(dir_t dir_r,game_mode_t mode,poss_dir_t* p){
    dir_t ret=dir_r;
    if(mode==HARD_MODE){
        ret=dir_r;
    }else if(mode==EASY_MODE){
        int idx=0;
        dir_t arr[10];
        for(int i = 0;i<10;++i){
            arr[i]=p->dirs[idx++];
            if(idx==p->num -1){
                idx=0;
            }
        }
        int k = rand() % 10;
        ret=arr[k];
    }
    return ret;
}

bool opposite_direction(dir_t dir1, dir_t dir2){
    bool ret = false;
    if((dir1==up && dir2==down)||(dir1==down && dir2==up)||
    (dir1==left && dir2==right)||(dir1==right && dir2==left)){
        ret=true;
    }
    return ret;
}

bool is_dir_possible(dir_t dir,snake_t *snake,snake_t *opp){
    cell_t *head = snake->head;
    dir_t dir_h=head->dir;
    bool ret=true;
    if(opposite_direction(dir_h,dir)){
        ret=false;
    }
    if(snake_next_snake(snake,opp,dir)){
        ret=false;
    }
    if(snake_next_himself(snake,dir)){
        ret=false;
    }
    return ret;
}

bool find_cell(int x, int y, snake_t *snake,snake_t *opp,game_mode_t mode){
    bool ret=false;
    cell_t *head = snake->head;
    int posx_h=head->x;
    int posy_h=head->y;
    dir_t new_dir;
    poss_dir_t* p;
    p=possible_directions(snake,opp);
    //printf("num of poss dir: %d\n",p->num);
    new_dir=none;
    if(p->num==0){
        new_dir=snake->head->dir;   //no possible move
        ret=false;
    }else if(p->num==1){   //one possible move
        new_dir=p->dirs[0];
        ret=true;
    }else{      //multible possible moves
        for(int i=0;i<(p->num);++i){
            switch(p->dirs[i]){
                case up:
                    if(y-posy_h<0){
                        new_dir=p->dirs[i];
                    }
                    break;
                case down:
                    if(y-posy_h>0){
                        new_dir=p->dirs[i];
                    }
                    break;
                case right:
                    if(x-posx_h>0){
                        new_dir=p->dirs[i];
                    }
                    break;
                case left:                 
                    if(x-posx_h<0){
                        new_dir=p->dirs[i];
                    }
                    break;
                default:
                    break;
            }
        }
        if(new_dir==none){
            new_dir=p->dirs[0];
        }
        new_dir=choose_dir_prob(new_dir,mode,p);
        ret=true;
    }
    snake_dir(snake,new_dir);
    free(p->dirs);
    free(p);
    return ret;

}