#include "gui.h"
#include "tile_manager.h"
#include <string.h>
#include <stdlib.h>
#include <time.h>

uint16_t* tiles[6];
uint16_t *no_select;
uint16_t *selected;
uint16_t *bckgnd[30*20];
uint16_t *bckgnd_t1;
uint16_t *bckgnd_t2;
uint16_t *bckgnd_t3;

void gui_init() {
	no_select = load_tile_ppm("assets/no_selection.ppm");
	selected = load_tile_ppm("assets/selection.ppm");
	
	bckgnd_t1 = load_tile_ppm("assets/bckgnd1.ppm");
	bckgnd_t2 = load_tile_ppm("assets/bckgnd2.ppm");
	bckgnd_t3 = load_tile_ppm("assets/bckgnd3.ppm");

	srand(time(NULL));
	//int r = rand();

	for (int x = 0; x < 30; ++x)
	for (int y = 0; y < 20; ++y) {
		switch (rand()%3) {
			case 0:
				bckgnd[x + y*20] = bckgnd_t1;
				break;
			case 1:
				bckgnd[x + y*20] = bckgnd_t2;
				break;
			case 2:
				bckgnd[x + y*20] = bckgnd_t3;
				break;


		}
	}

	for (int i = 0; i < 6; ++i) {
		tiles[i] = no_select;
	}
}

void gui_clear() {
	free(no_select);
	free(selected);
	free(bckgnd_t1);
	free(bckgnd_t2);
	free(bckgnd_t3);
}

void draw_settings(int sel, int len1, int len2, int input, int snake1, int snake2) {
	uint16_t clr[5];
	for (int i = 0; i < 5; ++i)
		clr[i] = WHITE;

	for (int i = 0; i < 5; ++i) {
		tiles[i] = no_select;
	}

	tiles[sel] = selected;

	clr[sel] = RED;	

	char len_s[5];

	// header
	print(60,20,"Settings", 2, WHITE);

	int x = 4;
	int y = 4;
	//
	draw_tile(x, y, tiles[0]);
	print(x*TILE_SIZE + TILE_SIZE*2, y*TILE_SIZE-4,"input", 2, clr[0]);
	if (input == 0)
		print(x*TILE_SIZE + TILE_SIZE*2 + 160, y*TILE_SIZE-4, "knobs relative", 2, clr[0]);
	else if (input == 1)
		print(x*TILE_SIZE + TILE_SIZE*2 + 160, y*TILE_SIZE-4, "keyboard", 2, clr[0]);
	else
		print(x*TILE_SIZE + TILE_SIZE*2 + 160, y*TILE_SIZE-4, "knobs absolute", 2, clr[0]);


	y += 2;

	//
	draw_tile(x, y, tiles[1]);
	print(x*TILE_SIZE + TILE_SIZE*2 ,y*TILE_SIZE-4,"player 1", 2, clr[1]);
	if (!snake1)
		print(x*TILE_SIZE + TILE_SIZE*2 + 180, y*TILE_SIZE-4, "SNAKE 1", 2, clr[1]);
	else if (snake1 == 1)
		print(x*TILE_SIZE + TILE_SIZE*2 + 180, y*TILE_SIZE-4, "SNAKE 2", 2, clr[1]);
	else
		print(x*TILE_SIZE + TILE_SIZE*2 + 180, y*TILE_SIZE-4, "SNAKE 3", 2, clr[1]);



	y += 2;
	
	//
	draw_tile(x, y, tiles[2]);
	print(x*TILE_SIZE + TILE_SIZE*2 ,y*TILE_SIZE-4,"player 2", 2, clr[2]);
	if (!snake2)
		print(x*TILE_SIZE + TILE_SIZE*2 + 180, y*TILE_SIZE-4, "SNAKE 1", 2, clr[2]);
	else if (snake2 == 1)
		print(x*TILE_SIZE + TILE_SIZE*2 + 180, y*TILE_SIZE-4, "SNAKE 2", 2, clr[2]);
	else
		print(x*TILE_SIZE + TILE_SIZE*2 + 180, y*TILE_SIZE-4, "SNAKE 3", 2, clr[2]);




	y += 2;



	//
	draw_tile(x, y, tiles[3]);
	print(x*TILE_SIZE + TILE_SIZE*2 ,y*TILE_SIZE-4,"snake length: ", 2, clr[3]);
	y += 2;

	sprintf(len_s, "P1 %d", len1);
	print(x*TILE_SIZE + TILE_SIZE*2 ,y*TILE_SIZE-4, len_s, 2, clr[3]);
	sprintf(len_s, "P2 %d", len2);
	print(x*TILE_SIZE + TILE_SIZE*10 ,y*TILE_SIZE-4, len_s, 2, clr[3]);
	
	y += 2;
	
	//
	draw_tile(x, y, tiles[4]);
	print(x*TILE_SIZE + TILE_SIZE*2 ,y*TILE_SIZE-4,"back ", 2, clr[4]);
}

void draw_game_over(char *msg) {
	print(112, 80, "GAME OVER", 4, RED);
	print(112, 144, msg, 3, YELLOW);
}

void draw_background() {
	for (int x = 0; x < 30; ++x)
	for (int y = 0; y < 20; ++y) {
		draw_tile(x, y, bckgnd[x + y*20]);
	}
}

void draw_menu(int sel) {
	//draw_tile(0,0,selected);
	uint16_t clr[6];
	for (int i = 0; i < 6; ++i)
		clr[i] = WHITE;

	for (int i = 0; i < 6; ++i) {
		tiles[i] = no_select;
	}

	tiles[sel] = selected;

	clr[sel] = RED;	

	// header
	print(60,20,"SNAKE GAME", 2, WHITE);

	int x = 4;
	int y = 4;
	//
	draw_tile(x, y, tiles[0]);
	print(x*TILE_SIZE + TILE_SIZE*2 ,y*TILE_SIZE-4,"singleplayer", 2, clr[0]);
	y += 2;

	//
	draw_tile(x, y, tiles[1]);
	print(x*TILE_SIZE + TILE_SIZE*2 ,y*TILE_SIZE-4,"multiplayer", 2, clr[1]);
	y += 2;

	//
	draw_tile(x, y, tiles[2]);
	print(x*TILE_SIZE + TILE_SIZE*2 ,y*TILE_SIZE-4,"player vs AI", 2, clr[2]);
	y += 2;
	
	//
	draw_tile(x, y, tiles[3]);
	print(x*TILE_SIZE + TILE_SIZE*2 ,y*TILE_SIZE-4,"AI vs AI", 2, clr[3]);
	y += 2;
	
	//
	draw_tile(x, y, tiles[4]);
	print(x*TILE_SIZE + TILE_SIZE*2 ,y*TILE_SIZE-4,"Settings", 2, clr[4]);
	y += 2;

	
	//
	draw_tile(x, y, tiles[5]);
	print(x*TILE_SIZE + TILE_SIZE*2 ,y*TILE_SIZE-4,"exit", 2, clr[5]);

}


void draw_editor() {
	uint16_t white[TILE_SIZE*TILE_SIZE];
	for (int i = 0; i < TILE_SIZE*TILE_SIZE; ++i)
		white[i] = -1;

	print(60,20,"TILE EDITOR", 1, WHITE);
	for (int x = 6; x <= 7+16; ++x) {
		draw_tile(x,1, white);
		draw_tile(x,2+16, white);
	}

	for (int y = 1; y <= 2 +16; ++y) {
		draw_tile(6,y, white);
		draw_tile(7+16,y, white);
	}

	/*for (int y = 3; y <3+16; ++y)
		for (int x = 7; x <7+16; ++x) {
			for (int i = 0; i < TILE_SIZE*TILE_SIZE; ++i)
				white[i] = tmp_buf[x + y * TILE_SIZE];

			draw_tile(x, y, white);
		}
	*/

	for (int i = 0; i < TILE_SIZE; ++i)
	for (int j = 0; j < TILE_SIZE; ++j) {
		draw_tile_color(i+7, j+2, get_tmp_clr(i, j));
		//printf("serwewwr %d\n", get_tmp_clr(i, j));
	}
}

void draw_selector(int x, int y, uint16_t clr) {
	uint16_t selector[TILE_SIZE*TILE_SIZE];
	for (int i = 0; i < TILE_SIZE*TILE_SIZE; ++i)
		if (i<TILE_SIZE || i >= (TILE_SIZE-1)*TILE_SIZE || !(i%TILE_SIZE) || (i%TILE_SIZE == TILE_SIZE-1))
			selector[i] = RED + GREEN;
		else
			selector[i] = clr;

	draw_tile(x, y, selector);


}
