
//#define __DEBUG__
#define _POSIX_C_SOURCE 200112L
#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <time.h>
#include <unistd.h>
#include "mzapo_parlcd.h"
#include "mzapo_phys.h"
#include "mzapo_regs.h"
#include "serialize_lock.h"
#include "lcd.h"
#include "input_thread.h"
#include "snake.h"
#include "utils.h"
#include "tile_manager.h"
#include "gui.h"
#include "ai.h"
#include "player2.h"
#include "knob_input.h"
#include "button_thread.h"
#include "led.h"

void handle_press(bool *btn, args_t *btns, struct timespec *loop_delay);

int main(int argc, char *argv[])
{

	led_init();

	init_knobs();
	init_random();
	result_t res;
	state_t state = GAME;

	args_t btns = { 0, 0, 0, 0 };
	kbd_args_t kbd_dir = { 0, 0, 0 };

	dir_t dir = none;

	void *(*thr_functions[])(void*) = { input_thread, button_thread
	};

	pthread_t threads[2];
	pthread_mutex_init(&mtx, NULL);	// initialize mutex with default attributes
	pthread_mutex_init(&btn_mtx, NULL);	// initialize mutex with default attributes

	int r;

	for (int i = 0; i < 2; ++i)
	{
		if (i == 0)
			r = pthread_create(&threads[i], NULL, thr_functions[i], &kbd_dir);
		if (i == 1)
			r = pthread_create(&threads[i], NULL, thr_functions[i], &btns);

		fprintf(stderr, "Create thread no. %d %s\n", i, (r == 0 ? "OK" : "FAIL"));
	}

	unsigned char *mem_base;

	mem_base = map_phys_address(SPILED_REG_BASE_PHYS, SPILED_REG_SIZE, 0);

	/*Serialize execution of applications */

	/*Try to acquire lock the first */
	if (serialize_lock(1) <= 0)
	{
		printf("System is occupied\n");

		if (1)
		{
			printf("Waitting\n");
			/*Wait till application holding lock releases it or exits */
			serialize_lock(0);
		}
	}

	printf("Hello world\n");
	//uint8_t k_blue;
	//uint8_t k_green;
	//uint8_t k_red;

	lcd_init();

	dir_t d1 = right;
	dir_t d2 = right;

	bool quit = false;

	bool first_cycle = true;

	//uint16_t *select = load_tile_ppm("paprika.ppm");

	snake_t *snake1 = NULL;
	snake_t *snake2 = NULL;
	cell_t *food = NULL;
	cell_t *objects[5];
	int snake1_len = 4;
	int snake2_len = 4;

	int x = 0, y = 0;

	x = 8;
	y = 3;

	uint16_t edit_buf[TILE_SIZE *TILE_SIZE];

	debug("ENTERING LOOP\n");

	dir_t e_d = none;
	dir_t p_d = none;

	state = MENU;

	gui_init();

	int sel;

	//lading asets
	uint16_t *head_up1 = load_tile_ppm("assets/yhead_up.ppm");
	uint16_t *head_down1 = load_tile_ppm("assets/yhead_down.ppm");
	uint16_t *head_left1 = load_tile_ppm("assets/yhead_left.ppm");
	uint16_t *head_right1 = load_tile_ppm("assets/yhead_right.ppm");
	uint16_t *head_up2 = load_tile_ppm("assets/head_up.ppm");
	uint16_t *head_down2 = load_tile_ppm("assets/head_down.ppm");
	uint16_t *head_left2 = load_tile_ppm("assets/head_left.ppm");
	uint16_t *head_right2 = load_tile_ppm("assets/head_right.ppm");
	uint16_t *head_up3 = load_tile_ppm("assets/rhead_up.ppm");
	uint16_t *head_down3 = load_tile_ppm("assets/rhead_down.ppm");
	uint16_t *head_left3 = load_tile_ppm("assets/rhead_left.ppm");
	uint16_t *head_right3 = load_tile_ppm("assets/rhead_right.ppm");
	uint16_t * snk_body_p1;
	uint16_t * snk_body_p2;
	uint16_t *snk_body1 = load_tile_ppm("assets/ybody.ppm");
	uint16_t *snk_body2 = load_tile_ppm("assets/body.ppm");
	uint16_t *snk_body3 = load_tile_ppm("assets/rbody.ppm");
	uint16_t *food_tile1 = load_tile_ppm("assets/cherry.ppm");
	uint16_t *food_tile2 = load_tile_ppm("assets/paprika.ppm");
	uint16_t *food_tile3 = load_tile_ppm("assets/test.ppm");
	uint16_t *food_tiles[3] = { food_tile1, food_tile2, food_tile3
	};

	uint16_t *heads1[] = { head_up1, head_down1, head_left1, head_right1
	};
	uint16_t *heads2[] = { head_up2, head_down2, head_left2, head_right2
	};
	uint16_t *heads3[] = { head_up3, head_down3, head_left3, head_right3
	};
	// ----

	bool red_button = false;
	bool green_button = false;
	bool blue_button = false;
	bool p_blue_button = false;

	int loop_wait_ms = 50;

	struct timespec loop_delay = {.tv_sec = 0, .tv_nsec = loop_wait_ms *1000 * 1000
	};

	int input_mode = 0;
	int base_input_mode = 0;

	snk_body_p1 = snk_body1;
	auto snk_head_p1 = heads1;
	snk_body_p2 = snk_body2;
	auto snk_head_p2 = heads2;

	int snake_type_p2 = 1;
	int snake_type_p1 = 1;

	dir_t player1_input = none;
	dir_t player2_input = none;

	int base_snake1_len = 0;
	int base_snake2_len = 0;

	int p_sel = 0;

	while (!quit)
	{

		pthread_mutex_lock(&mtx);

		d1 = kbd_dir.dir1 == none ? d1 : kbd_dir.dir1;
		d2 = kbd_dir.dir2 == none ? d2 : kbd_dir.dir2;
		quit |= kbd_dir.quit;
		kbd_dir.quit |= quit;
		pthread_mutex_unlock(&mtx);

		pthread_mutex_lock(&btn_mtx);
		red_button = btns.r_btn;
		green_button = btns.g_btn;
		blue_button = btns.b_btn;
		btns.r_btn = false;
		btns.g_btn = false;
		btns.b_btn = false;
		btns.quit |= quit;
		pthread_mutex_unlock(&btn_mtx);

		clear_screen();

		//k_blue = *(volatile uint8_t *)(mem_base + SPILED_REG_KNOBS_8BIT_o);
		//k_green = *(volatile uint8_t *)(mem_base + SPILED_REG_KNOBS_8BIT_o + 1);
		//k_red = *(volatile uint8_t *)(mem_base + SPILED_REG_KNOBS_8BIT_o + 2);

		//get_dir_abs();
		if (red_button && green_button && blue_button)
		{
			state = MENU;
			handle_press(&red_button, &btns, &loop_delay);
		}

		switch (state)
		{

			case SETTINGS:
				sel = get_select() % 5;
				draw_settings(sel, snake1_len, snake2_len, input_mode, snake_type_p1, snake_type_p2);
				if (sel == 3)
				{
					if (p_sel != sel)
					{
						base_snake1_len = snake1_len - (get_val(1) % 16) - 2;
						base_snake2_len = snake2_len - (get_val(2) % 16) - 2;
					}
					snake1_len = ((get_val(1) % 16) + base_snake1_len + 32) % 16 + 2;
					snake2_len = ((get_val(2) % 16) + base_snake2_len + 32) % 16 + 2;

					p_sel = sel;
					break;
				}
				p_sel = sel;
				if (sel == 0)
				{
					if (blue_button > p_blue_button)
					{
						if (input_mode == 0)
						{
							input_mode = 1;
						}
						else if (input_mode == 1)
						{
							input_mode = 2;
						}
						else if (input_mode == 2)
						{
							input_mode = 0;
						}
					}
					p_blue_button = blue_button;
					break;
				}
				if (sel == 1)
				{
					if (blue_button > p_blue_button)
					{
						if (snake_type_p1 == 0)
						{
							snk_body_p1 = snk_body1;
							snk_head_p1 = heads1;
							snake_type_p1 = 1;
						}
						else if (snake_type_p1 == 1)
						{
							snk_body_p1 = snk_body2;
							snk_head_p1 = heads2;
							snake_type_p1 = 2;
						}
						else if (snake_type_p1 == 2)
						{
							snk_body_p1 = snk_body3;
							snk_head_p1 = heads3;
							snake_type_p1 = 0;
						}
					}
					p_blue_button = blue_button;
					break;
				}
				if (sel == 2)
				{
					if (blue_button > p_blue_button)
					{
						if (snake_type_p2 == 0)
						{
							snk_body_p2 = snk_body1;
							snk_head_p2 = heads1;
							snake_type_p2 = 1;
						}
						else if ((snake_type_p2 == 1))
						{
							snk_body_p2 = snk_body2;
							snk_head_p2 = heads2;
							snake_type_p2 = 2;
						}
						else if (snake_type_p2 == 2)
						{
							snk_body_p2 = snk_body3;
							snk_head_p2 = heads3;
							snake_type_p2 = 0;
						}
					}
					p_blue_button = blue_button;
					break;
				}

				if (red_button /*is_pressed()*/)
				{
					switch (sel)
					{
						case 0:

							break;
						case 1:
							break;
						case 3:
							snake1_len = (get_val(2) % 16) + 2;
							break;
						case 4:
							state = MENU;
							handle_press(&red_button, &btns, &loop_delay);
							red_button = false;
							for (int i = 0; i < 4; ++i)
								clock_nanosleep(CLOCK_MONOTONIC, 0, &loop_delay, NULL);

							red_button = false;
							break;
					}
				}
				break;

			case MENU:
				sel = get_select() % 6;
				draw_menu(sel);
				if (red_button /*is_pressed()*/)
				{
					switch (sel)
					{
						case 0:
							state = GAME_SINGLE;
							break;
						case 1:
							state = GAME_TWO_PLAYERS;
							break;
						case 2:
							state = GAME_VS_AI;
							break;
						case 3:
							state = GAME_TWO_AI;
							break;

						case 4:
							state = SETTINGS;
							handle_press(&red_button, &btns, &loop_delay);
							red_button = false;
							break;
						case 5:
							pthread_mutex_lock(&mtx);
							btns.quit = true;
							kbd_dir.quit = true;
							quit = true;
							pthread_mutex_unlock(&mtx);
							break;
					}
					//init game
					switch (state)
					{
						case GAME_SINGLE:

							if (snake1 != NULL)
							{
								kill_snake(&snake1);
							}
							if (snake2 != NULL)
							{
								kill_snake(&snake2);
							}

							snake1 = spawn_snake(5, 5, snk_body_p1, snk_head_p1, snake1_len);
							snake2 = NULL;
							if (food != NULL)
								free(food);
							food = spawn_cell(10, 15, 1, food_tiles);
							update_food(food, snake1, snake2);
							objects[0] = food;
							objects[1] = snake1->head;
							objects[2] = NULL;
							first_cycle = true;
							break;
						case GAME_TWO_PLAYERS:
						case GAME_VS_AI:
						case GAME_TWO_AI:

							if (snake1 != NULL)
								kill_snake(&snake1);
							if (snake2 != NULL)
								kill_snake(&snake2);

							snake1 = spawn_snake(5, 5, snk_body_p1, snk_head_p1, snake1_len);
							snake2 = spawn_snake(5, 15, snk_body_p2, snk_head_p2, snake2_len);
							if (food != NULL)
								free(food);
							food = spawn_cell(10, 15, 1, food_tiles);
							update_food(food, snake1, snake2);
							objects[0] = food;
							objects[1] = snake1->head;
							objects[2] = snake2->head;
							objects[3] = NULL;
							first_cycle = true;
							break;
						default:
							break;
					}
				}
				break;

			case GAME_SINGLE:
			case GAME_TWO_PLAYERS:

				switch (input_mode)
				{
					case 0:
						player1_input = get_dir(0);
						player2_input = get_dir(2);
						break;
					case 1:
						player1_input = d1;
						player2_input = d2;
						break;
					case 2:
						player1_input = get_dir_abs_red();
						player2_input = get_dir_abs_blue();
						break;
				}
				//player_input = get_dir(0);
				draw_background();
				led_line_left(snake1->length);
				if (snake2 != NULL)
				{
					led_line_right(snake2->length);
				}
				//get_dir_abs();
				if (player_v_player(snake1, snake2, food, player1_input, player2_input, objects, &res, &first_cycle))
				{
					printf("END OF GAME\n");

					switch (res)
					{
						case P1:
							draw_game_over("PLAYER 1 WINS");
							break;
						case P2:
							if (state == GAME_SINGLE)
								draw_game_over("YOU ARE A LOSER");
							else
								draw_game_over("PLAYER 2 WINS");
							break;
						case D:
							draw_game_over("DRAW");
							break;
					}
					lcd_refresh();
					handle_press(&red_button, &btns, &loop_delay);

					led_line_clear();
					first_cycle = true;
					state = MENU;
					//quit=true;
					break;
				}
				break;
			case GAME_VS_AI:

				switch (input_mode)
				{
					case 0:
						player1_input = get_dir(0);
						break;
					case 1:
						player1_input = d1;
						break;
					case 2:
						player1_input = get_dir_abs_red();
						break;
				}
				draw_background();

				led_line_left(snake1->length);
				led_line_right(snake2->length);
				if (player_v_ai(snake1, snake2, food, player1_input, HARD_MODE, objects, &res, &first_cycle))
				{
					printf("END OF GAME\n");

					switch (res)
					{
						case P1:
							draw_game_over("PLAYER 1 WINS");
							break;
						case P2:
							draw_game_over("PLAYER 2 WINS");
							break;
						case D:
							draw_game_over("DRAW");
							break;
					}
					lcd_refresh();
					led_line_clear();
					handle_press(&red_button, &btns, &loop_delay);
					first_cycle = true;
					state = MENU;
					break;
				}
				break;
			case GAME_TWO_AI:

				led_line_left(snake1->length);
				led_line_right(snake2->length);
				draw_background();
				if (ai_v_ai(snake1, snake2, food, HARD_MODE, objects, &res, &first_cycle))
				{
					printf("END OF GAME\n");

					switch (res)
					{
						case P1:
							draw_game_over("PLAYER 1 WINS");
							break;
						case P2:
							draw_game_over("PLAYER 2 WINS");
							break;
						case D:
							draw_game_over("DRAW");
							break;
					}
					lcd_refresh();
					led_line_clear();
					handle_press(&red_button, &btns, &loop_delay);
					first_cycle = true;
					state = MENU;
					break;
				}
				break;
			default:
				break;
		}

		lcd_refresh();

		clock_nanosleep(CLOCK_MONOTONIC, 0, &loop_delay, NULL);
	}	//end while

	clear_screen();
	lcd_refresh();

	lcd_destroy();

	for (int i = 0; i < 2; ++i)
	{
		fprintf(stderr, "Call join to the thread no. %d\n", i);
		//if () {}
		if (i == 0)
			printf("Please press a random key to exit!\n");
		int r = pthread_join(threads[i], NULL);
		fprintf(stderr, "Joining the thread %d has been %s\n", i, (r == 0 ? "OK" : "FAIL"));
	}

	//unloading asets
	free(head_up1);
	free(head_down1);
	free(head_left1);
	free(head_right1);
	free(head_up2);
	free(head_down2);
	free(head_left2);
	free(head_right2);
	free(head_up3);
	free(head_down3);
	free(head_left3);
	free(head_right3);
	free(snk_body1);
	free(snk_body2);
	free(snk_body3);
	free(food_tile1);
	free(food_tile2);
	free(food_tile3);
	// ----

	if (snake1 != NULL)
		kill_snake(&snake1);
	if (snake2 != NULL)
		kill_snake(&snake2);

	if (food != NULL)
		free(food);

	led_line_clear();

	*(volatile uint32_t *)(mem_base + SPILED_REG_LED_RGB1_o) = 0x000000;

	printf("SNAKE PROGRAM ENDED\n");

	/*Release the lock */
	serialize_unlock();

	return 0;
}

void handle_press(bool *btn, args_t *btns, struct timespec *loop_delay)
{
	while (!(*btn) /*!is_pressed()*/)
	{
		pthread_mutex_lock(&btn_mtx);
		*btn = btns->r_btn;
		btns->r_btn = false;
		pthread_mutex_unlock(&btn_mtx);

		//for (int i = 0; i < 4; ++i)
		clock_nanosleep(CLOCK_MONOTONIC, 0, loop_delay, NULL);
	}
	while (*btn /*is_pressed()*/)
	{
		pthread_mutex_lock(&btn_mtx);
		*btn = btns->r_btn;
		btns->r_btn = false;
		pthread_mutex_unlock(&btn_mtx);

		//for (int i = 0; i < 4; ++i)
		clock_nanosleep(CLOCK_MONOTONIC, 0, loop_delay, NULL);
	}
}
