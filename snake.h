#pragma once

#include "lcd.h"
#include "utils.h"
#include <stdbool.h>

/*typedef enum {
	SNAKE,
	FOOD,
	WALL
} type_t;

typedef union {
	type_t type;
	int team
} kind_t;
*/

typedef struct cell_s {
	int type;          // 0- = undefined; 1 = food; 2 = wall; 3+ = snake no.
	dir_t dir;
	int x;
	int y;
	bool head;
	bool tail;
	struct cell_s* next;
	uint16_t* tile;
	uint16_t** tiles;
} cell_t;

typedef struct {
	cell_t* head;
	int length;
	uint16_t *body_tile;
	uint16_t **head_tiles;
} snake_t;

void food_tile_rand(cell_t* food);

void move_snake(snake_t *snake,dir_t dir);

void increase_length(snake_t* snake);

void update_snake(snake_t *snake);

void update_snake(snake_t*);

void snake_dir(snake_t* snake, dir_t dir);

void draw_snake(snake_t* snake);

snake_t* spawn_snake(int x, int y, uint16_t *body, uint16_t **head,int len);

void kill_snake(snake_t** snake);

bool collision(cell_t **cell);

cell_t* spawn_cell(int x, int y, int type, uint16_t *tiles[]);

void draw_cell(cell_t* cell);
