#pragma once

#include <stdint.h>

static uint8_t* mem_base;

void led_init();
void led_line_clear();
void led_line_left(uint8_t i);
void led_line_right(uint8_t i);
