#include "lcd.h"

static uint16_t *fb;

static uint8_t *parlcd_mem_base;

static int scale = 2;

font_descriptor_t *fdes = &font_rom8x16; //font_winFreeSystem14x16;

#define GREEN 0b0000011111100000
#define RED   0b1111100000000000
#define BLUE  0b0000000000011111

void lcd_destroy() {
  free(fb);
}

void clear_screen() {
  for (int i = 0; i < LCD_WIDTH*LCD_HEIGHT; ++i)
    fb[i] = 0;
}

void draw_pixel(int x, int y, uint16_t color) {
  if (y < LCD_HEIGHT && x < LCD_WIDTH && y >= 0 && x >= 0)
    fb[y*LCD_WIDTH + x] = color;
}

void draw_tile(int x, int y, uint16_t *tile) {
  if (x > LCD_WIDTH/TILE_SIZE || y > LCD_HEIGHT/TILE_SIZE)
	return;
  for(int i = 0; i < TILE_SIZE; ++i){
    for(int j = 0; j < TILE_SIZE; ++j){
      if (tile[i + j*TILE_SIZE])
        draw_pixel(TILE_SIZE*x + i, TILE_SIZE*y + j, tile[i + j*TILE_SIZE]);
    }
  }
}

void draw_tile_color(int x, int y, uint16_t clr) {
  if (x > LCD_WIDTH/TILE_SIZE || y > LCD_HEIGHT/TILE_SIZE)
	return;
  for(int i = 0; i < TILE_SIZE; ++i){
    for(int j = 0; j < TILE_SIZE; ++j){
      draw_pixel(TILE_SIZE*x + i, TILE_SIZE*y + j, clr);
    }
  }
}

void lcd_refresh() {
  parlcd_write_cmd(parlcd_mem_base, 0x2c);
  for (int i = 0; i < LCD_HEIGHT ; i++) {
    for (int j = 0; j < LCD_WIDTH ; j++) {
      parlcd_write_data(parlcd_mem_base, fb[i*LCD_WIDTH + j]);
    }
  }
}

void lcd_init() {
  parlcd_mem_base = map_phys_address(PARLCD_REG_BASE_PHYS, PARLCD_REG_SIZE, 0);
  parlcd_hx8357_init(parlcd_mem_base);
  parlcd_write_cmd(parlcd_mem_base, 0x2c);
  fb = malloc(LCD_HEIGHT*LCD_WIDTH*sizeof(uint16_t));
  for (int i = 0; i < LCD_WIDTH ; i++) {
    for (int j = 0; j < LCD_HEIGHT ; j++) {
      parlcd_write_data(parlcd_mem_base, 0);
    }
  }
}

void draw_pixel_big(int x, int y, unsigned short color) {
  int i,j;
  for (i=0; i<scale; i++) {
    for (j=0; j<scale; j++) {
      draw_pixel(x+i, y+j, color);
    }
  }
}

int char_width(int ch) {
  int width;
  if (!fdes->width) {
    width = fdes->maxwidth;
  } else {
    width = fdes->width[ch-fdes->firstchar];
  }
  return width;
}

void draw_char(int x, int y, char ch, unsigned short color) {
  int w = char_width(ch);
  const font_bits_t *ptr;
  if ((ch >= fdes->firstchar) && (ch-fdes->firstchar < fdes->size)) {
    if (fdes->offset) {
      ptr = &fdes->bits[fdes->offset[ch-fdes->firstchar]];
    } else {
      int bw = (fdes->maxwidth+15)/16;
      ptr = &fdes->bits[(ch-fdes->firstchar)*bw*fdes->height];
    }
    int i, j;
    for (i=0; i<fdes->height; i++) {
      font_bits_t val = *ptr;
      for (j=0; j<w; j++) {
        if ((val&0x8000)!=0) {
          draw_pixel_big(x+scale*j, y+scale*i, color);
        }
        val<<=1;
      }
      ptr++;
    }
  }
}

void print(int x, int y, const char* str, int s, uint16_t clr) {
	scale = s;
	for (int i = 0; str[i] != '\0'; ++i) {
		draw_char(x, y, str[i], clr);
		x += scale*char_width(str[i]);
	}
}
