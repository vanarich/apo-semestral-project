#pragma once
#include "snake.h"
#include "utils.h"
#include <stdbool.h>
#include "ai.h"


typedef enum{
	C_SNAKE1_FOOD,
	C_SNAKE2_FOOD,
	C_SNAKE1,
	C_SNAKE2,
	C_SNAKE1_to_SNAKE2,
	C_SNAKE2_to_SNAKE1,
    C_HEAD,
    C_OTHER
}collision_t;

typedef enum{
    P1,
    P2,
    D
}result_t;


//bool ai(snake_t* snake1,cell_t* food,game_mode_t mode,cell_t **cell_buf,result_t* res,bool* fc);

void update_food(cell_t* food, snake_t *snake1, snake_t *snake2);

bool snake_bite_snake(snake_t* snake1, snake_t* snake2);

collision_t decode_collision(snake_t* snake1, snake_t* snake2,cell_t* food);

bool ai_v_ai(snake_t* snake1, snake_t* snake2,cell_t* food,game_mode_t mode,cell_t **cell_buf,result_t* res,bool* fc);

bool player_v_ai(snake_t* snake1, snake_t* snake2,cell_t* food,dir_t dir,game_mode_t mode,cell_t **cell_buf,result_t* res,bool* fc);

bool player_v_player(snake_t* snake1, snake_t* snake2,cell_t* food,dir_t dir1,dir_t dir2,cell_t **cell_buf,result_t* res,bool* fc);

