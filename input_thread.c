#include "input_thread.h"
#include "utils.h"
#include <stdint.h>
#include <poll.h>

//union dir_t dir = none;

pthread_mutex_t mtx;

bool quit_all = false;

dir_t dir2 = none;

dir_t dir3 = none;

bool enter_stroke = false;

bool g_stroke = false;

int getc_timeout(int timeout_ms, uint8_t *c) {
	struct pollfd ufdr[1];
	int r = 0;
	ufdr[0].fd = 0;
	ufdr[0].events = POLLIN | POLLRDNORM;
	if ((poll(&ufdr[0], 1, timeout_ms) > 0) && (ufdr[0].revents & (POLLIN | POLLRDNORM))) {
		r = read(0, c, 1);
	}
	return r;
}

void call_termios(int reset)
{
   static struct termios tio, tioOld;
   tcgetattr(STDIN_FILENO, &tio);
   if (reset) {
      tcsetattr(STDIN_FILENO, TCSANOW, &tioOld);
   } else {
      tioOld = tio; //backup 
      cfmakeraw(&tio);
      tio.c_oflag |= OPOST;
      tcsetattr(STDIN_FILENO, TCSANOW, &tio);
   }
}

void* input_thread(void* v_kbd_dir) {
	kbd_args_t *kbd_dir = v_kbd_dir;
	char c = 0;
	bool quit = false;
	call_termios(0);

	while (!quit && (c = getchar())) {
		pthread_mutex_lock(&mtx);
		quit = kbd_dir->quit;
		pthread_mutex_unlock(&mtx);

		switch (c) {
			case 'r':
				pthread_mutex_lock(&mtx);
				enter_stroke = true;
				pthread_mutex_unlock(&mtx);
				break;
			case 'g':
				pthread_mutex_lock(&mtx);
				g_stroke = true;
				pthread_mutex_unlock(&mtx);
				break;

			case 'w':
				pthread_mutex_lock(&mtx);
				kbd_dir->dir1 = up;
				//dir2 = *dir;
				pthread_mutex_unlock(&mtx);
				break;

			case 'a':
				pthread_mutex_lock(&mtx);
				kbd_dir->dir1 = left;
				//dir2 = *dir;
				pthread_mutex_unlock(&mtx);
				break;
			
			case 's':
				pthread_mutex_lock(&mtx);
				kbd_dir->dir1 = down;
				//dir2 = *dir;
				pthread_mutex_unlock(&mtx);
				break;

			case 'd':
				pthread_mutex_lock(&mtx);
				kbd_dir->dir1 = right;
				//dir2 = *dir;
				pthread_mutex_unlock(&mtx);
				break;
			
			case 'i':
				pthread_mutex_lock(&mtx);
				kbd_dir->dir2 = up;
				//dir2 = *dir;
				pthread_mutex_unlock(&mtx);
				break;

			case 'j':
				pthread_mutex_lock(&mtx);
				kbd_dir->dir2 = left;
				//dir2 = *dir;
				pthread_mutex_unlock(&mtx);
				break;
			
			case 'k':
				pthread_mutex_lock(&mtx);
				kbd_dir->dir2 = down;
				//dir2 = *dir;
				pthread_mutex_unlock(&mtx);
				break;

			case 'l':
				pthread_mutex_lock(&mtx);
				kbd_dir->dir2 = right;
				//dir2 = *dir;
				pthread_mutex_unlock(&mtx);
				break;

			case '\r':
				pthread_mutex_lock(&mtx);
				kbd_dir->dir1 = none;
				kbd_dir->dir2 = none;
				pthread_mutex_unlock(&mtx);
				break;

			case 'q':
				quit = true;
				pthread_mutex_lock(&mtx);
				kbd_dir->quit = true;
				pthread_mutex_unlock(&mtx);
				break;

			default:
				break;
		}
	}
	call_termios(1);
	return NULL;
}

