#pragma once

#include <stdbool.h>
#include <pthread.h>
#include <termios.h>
#include <unistd.h>
#include <stdio.h>
#include "utils.h"

extern pthread_mutex_t btn_mtx;

void* button_thread(void *);
