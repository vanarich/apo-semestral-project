#include "snake.h"
#include <time.h>
#include <stdlib.h>


void food_tile_rand(cell_t* food) {
	food->tile = food->tiles[rand()%3];
}

void increase_length(snake_t* snake){
    cell_t *head=snake->head;
	//printf("here\n");
    for (int i=0; i<(snake->length)-1; ++i){
		head = head->next;
    }
	//printf("here2\n");
    cell_t *cell = my_alloc(sizeof(cell_t));
    head->next = cell;
    head->next->dir=none;
    head->next->x=head->x;
    head->next->y=head->y;
    head->next->next=NULL;
    head->next->tile = snake->body_tile;
    snake->length++;
}

void move_cell(cell_t *cell) {
	dir_t dir = cell->dir;
	switch (dir) {
		case up:
			cell->y--;
			cell->y+=20;
			cell->y = cell->y % 20;
			break;
		case down:
			cell->y++;
			cell->y+=20;
			cell->y = cell->y % 20;
			break;
		case right:
			cell->x++;
			cell->x+=30;
			cell->x = cell->x % 30;
			break;
		case left:
			cell->x--;
			cell->x+=30;
			cell->x = cell->x % 30;
			break;
		case none:
		default:
			break;
	}
}

void update_snake(snake_t *snake) {
	
	cell_t *head = snake->head;
	dir_t dir = head->dir;
	dir_t prev_dir = dir;
	while (head != NULL) {
		dir = head->dir;
		
		move_cell(head);	

		head->dir = prev_dir;
		prev_dir = dir;
		head = head->next;
	}

	snake->head->tile = snake->head_tiles[snake->head->dir -1];
}

void snake_dir(snake_t *snake, dir_t dir) {
	cell_t *head = snake->head;

	if ((head->dir == up && dir == down) || (head->dir == down && dir == up) || (head->dir == left && dir == right) || (head->dir == right && dir == left))
		return;

	while (head != NULL && head->dir == none) {
		head->dir = dir;
		head = head->next;
	}

	if (head == snake->head)
		head->dir = dir;
}

void draw_snake(snake_t *snake) {
	uint16_t RED_TILE[16*16];
		for (int i = 0; i < 16*16; ++i) {
			RED_TILE[i] = 0b1111100000000000;
		}
	if (snake == NULL)
		return;
	for (cell_t *head = snake->head; head != NULL; head = head->next) {
		draw_tile(head->x, head->y, head->tile);
	}
}

cell_t* spawn_cell(int x, int y, int type, uint16_t *tiles[]) { 
	cell_t* cell = my_alloc(sizeof(cell_t));

	cell->x = x;

	cell->y = y;

	cell->type = type;

	cell->tile = tiles[0];

	cell->tiles = tiles;
	
	cell->next = NULL;

	return cell;
}

void draw_cell(cell_t* cell) {
	draw_tile(cell->x, cell->y, cell->tile);
}

// DONT FORGET TO KILL THE CELL WITH free()

snake_t* spawn_snake(int x, int y, uint16_t *body_tile, uint16_t **head_tile, int len) {
	snake_t* snake = my_alloc(sizeof(snake_t));
	cell_t *head = my_alloc(sizeof(cell_t));
	snake->head = head;
	snake->length=1;
	snake->body_tile = body_tile;
	snake->head_tiles = head_tile;

	head->x = x;
	head->y = y;
	head->dir = none;
	head->type  = 3;

	for (int i = 0; i < len-1; ++i) {
		cell_t *cell = my_alloc(sizeof(cell_t));

		cell->y = head->y;
		cell->x = head->x - 1;
		cell->dir = none;
		cell->tile = body_tile;

		head->next = cell;
		head = head->next;
		head->next = NULL;
		snake->length++;
	}
	return snake;
}

void kill_snake(snake_t** snake) {
	if (*snake == NULL)
		return;
	//if (snake->head == NULL)
	//	return;
	cell_t *next;
	for (cell_t *head = (*snake)->head; head != NULL; head = next) {
		next = head->next;
		free(head);
		debug("free cell");
		head = NULL;
	}
	debug("free snake");
	free(*snake);
	*snake = NULL;
}

bool collision(cell_t **cell_buf) {
	for (int i = 0; cell_buf[i] != NULL; ++i) {
		//printf("i: %d\n", i);

		cell_t *cell_i = cell_buf[i];

		do {
			for (int j = 0; cell_buf[j] != NULL; ++j) {
				//printf("j: %d\n", j);

				cell_t *cell_j = cell_buf[j];

				do {
					if (cell_j != cell_i) {
						if (cell_j->x == cell_i->x && cell_j->y == cell_i->y){
							//here colission
							return true;
						}
					}
					if (cell_j->next != NULL) {
						cell_j = cell_j->next;
						//printf("next cell\n", i);
					} else
						break;
				} while (1);
			}
			if (cell_i->next != NULL) {
				cell_i = cell_i->next;
			} else
				break;

		} while (1);
	}

	return NULL;
}

void move_snake(snake_t *snake,dir_t dir){
	snake_dir(snake, dir);
    update_snake(snake);
	draw_snake(snake);
}
