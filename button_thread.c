#include "button_thread.h"

#include <stdint.h>
#include "mzapo_phys.h"
#include "mzapo_regs.h"

pthread_mutex_t btn_mtx;

void* button_thread(void *v_args) {
	
	uint8_t *mem_base = map_phys_address(SPILED_REG_BASE_PHYS, SPILED_REG_SIZE, 0);

	args_t *args = v_args;
	bool quit = false;
	struct timespec loop_delay;
	loop_delay.tv_sec = 0;
	loop_delay.tv_nsec = 2 * 1000 * 1000;

	bool btns[3] = {0,0,0};

	while(!quit) {
		pthread_mutex_lock(&btn_mtx);

		uint8_t btns = *(volatile uint8_t*)(mem_base + SPILED_REG_KNOBS_8BIT_o + 3);

		//if )
		args->r_btn = btns & (1<<2);
		args->g_btn = btns & (1<<1);
		args->b_btn = btns & (1<<0);


		quit = args->quit;
		pthread_mutex_unlock(&btn_mtx);

		clock_nanosleep(CLOCK_MONOTONIC, 0, &loop_delay, NULL);
	}

	return NULL;
}
