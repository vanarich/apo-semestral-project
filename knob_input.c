#include "knob_input.h"
#include "mzapo_phys.h"
#include "mzapo_regs.h"
#include "utils.h"
#include <stdint.h>

static uint8_t *mem_base;

static uint8_t r_offset;
static uint8_t g_offset;
static uint8_t b_offset;

void init_knobs() {
	mem_base = map_phys_address(SPILED_REG_BASE_PHYS, SPILED_REG_SIZE, 0);
	r_offset = *(volatile uint8_t*)(mem_base + SPILED_REG_KNOBS_8BIT_o+2) + 10;
	b_offset = *(volatile uint8_t*)(mem_base + SPILED_REG_KNOBS_8BIT_o) + 10 - 20;

}

dir_t get_dir_abs_red() {
	static long long k_base = 0;
	//static int16_t p_k_red = ;
	uint8_t raw_k_red = *(volatile uint8_t*)(mem_base + SPILED_REG_KNOBS_8BIT_o+2);
	long long k_red = raw_k_red;
	static long long p_k_red = 10;

	k_red += k_base;

	if (p_k_red - k_red > 127) {
		k_base += 256;
		k_red += 256;
	} else if (p_k_red - k_red < -127) {
		k_base -= 256;
		k_red -= 256;
	}


	int delta = (int)r_offset - (int)k_red;
	while (delta >= 80)
		delta -= 80;
	while (delta < 0)
		delta += 80;
	delta /= 20;
	
	//printf("DIR %d %d %d\n", (delta)%4, delta, k_red);
	p_k_red = k_red;


	switch (delta%4) {
    		case 3:
			return right;
		case 0:
			return up;
		case 1:
			return left;
		case 2:
			return down;
        }
}

dir_t get_dir_abs_blue() {
	static long long k_base = 0;
	//static int16_t p_k_red = ;
	uint8_t raw_k_red = *(volatile uint8_t*)(mem_base + SPILED_REG_KNOBS_8BIT_o);
	long long k_red = raw_k_red;
	static long long p_k_red = 10;

	k_red += k_base;

	if (p_k_red - k_red > 127) {
		k_base += 256;
		k_red += 256;
	} else if (p_k_red - k_red < -127) {
		k_base -= 256;
		k_red -= 256;
	}


	int delta = (int)b_offset - (int)k_red;
	while (delta >= 80)
		delta -= 80;
	while (delta < 0)
		delta += 80;
	delta /= 20;
	
	//printf("DIR %d %d %d\n", (delta)%4, delta, k_red);
	p_k_red = k_red;


	switch (delta%4) {
    		case 2:
			return down;
		case 3:
			return right;
		case 0:
			return up;
		case 1:
			return left;

    }
}

dir_t get_dir(int knob) {
	
    uint8_t k_knob;	

    switch (knob) {
    	case 0:
		
    		k_knob = *(volatile uint8_t*)(mem_base + SPILED_REG_KNOBS_8BIT_o+2);
		break;
	case 1:
		
    		k_knob = *(volatile uint8_t*)(mem_base + SPILED_REG_KNOBS_8BIT_o+1);
		break;
	case 2:
				
    		k_knob = *(volatile uint8_t*)(mem_base + SPILED_REG_KNOBS_8BIT_o);
		break;
    }

    //printf("R: %d, Ro: %d, delta: %d\n", k_red, r_offset, (int)k_red - (int)r_offset);
   
    switch ((k_knob/8)%4) {
    	case 0:
		return left;
	case 1:
		return up;
	case 2:
		return right;
	case 3:
		return down;

    }
}

int get_val(int i) {

    uint8_t k_red = *(volatile uint8_t*)(mem_base + SPILED_REG_KNOBS_8BIT_o+2);
    uint8_t k_green = *(volatile uint8_t*)(mem_base + SPILED_REG_KNOBS_8BIT_o + 1);
    uint8_t k_blue = *(volatile uint8_t*)(mem_base + SPILED_REG_KNOBS_8BIT_o);
    
    if (i == 0) 
    	return (k_red/4);   
    else if (i == 1) 
    	return (k_green/4);
    else
    	return (k_blue/4);
}

int get_select() {
	static int16_t k_base = 0;
	uint8_t raw_k_red = *(volatile uint8_t*)(mem_base + SPILED_REG_KNOBS_8BIT_o+2);
	int16_t k_red = raw_k_red;
	static int16_t p_k_red = 10;

	k_red += k_base;

	if (p_k_red - k_red > 127) {
		k_base += 256;
		k_red += 256;
	} else if (p_k_red - k_red < -127) {
		k_base -= 256;
		k_red -= 256;
	}
	
	p_k_red = k_red;

	while (k_red < 0)
		k_red+=5*3*256;
    	
	return (k_red/4);
}

// DO NOT USE
bool is_pressed() {
	uint8_t btns = *(volatile uint8_t*)(mem_base + SPILED_REG_KNOBS_8BIT_o + 3);

	bool btn = btns & 0b100;
	return btn;
}

